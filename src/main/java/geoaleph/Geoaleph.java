package geoaleph;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchEvent.Modifier;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;
import org.omg.CORBA.Environment;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterResponse;
import twitter4j.auth.AccessToken;
import twitter4j.auth.Authorization;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.conf.ConfigurationContext;

public class Geoaleph
{
	public static double gridCellSize = 0.01;
	public static int gridSize = 16;
	public static int scaledImageSize = 500;
	public static int mapZoom = 16;
	
	public static double lowestPoint = -2, highestPoint = 2;
	public static double waterDropHigh = 0, waterDropLow = -0.04;

	static double latitude = 17.75, longitude =  -142.5;
	static boolean looping = true;
	
	public static void main(String[] args)
	{
		HeightMapGetter heightMapGetter = new HeightMapGetter();
		IHeightmapOutputter heightmapOutputter = new TextfileOutputter();
		
		try 
		{
			String configData = new String(Files.readAllBytes(Paths.get("settings.conf")));			
			JSONObject config = new JSONObject(configData);
			JSONObject twitterJson = config.getJSONObject("twitterConf");
			
			TwitterHandler twitterHandler = makeTwitterHandler(twitterJson);
			
			do {
				randomizePosition();
				JSONObject responseJSON = heightMapGetter.getHeightmap(latitude, longitude);
				double[] heightmap = heightMapGetter.convertJSONHeightmapToDoubleArray(responseJSON);
				if (!isHeightmapNotFlat(heightmap)) {
					heightmapOutputter.render(heightmap);
					File f = render();
					tweetImage(twitterHandler, f);
					waitUntilNextTweet();
				} else {
					System.out.println("oops! too flat");
				}
			} while (looping);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void tweetImage(TwitterHandler twitterHandler, File f)
	{
		try
		{
			twitterHandler.postImage(f, latitude, longitude);
		} 
		catch (TwitterException e)
		{
			e.printStackTrace();
		}
	}

	private static TwitterHandler makeTwitterHandler(JSONObject config)
	{
		try
		{
			return TwitterHandler.create(config);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private static void waitUntilNextTweet()
	{
		try
		{
			Thread.sleep(3600000);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	private static File render()
	{
		Process p = null;
		File f = null;
		try
		{
			 if (System.getProperty("os.name").contains("win")) {
				 p = Runtime.getRuntime().exec("cmd /c render.bat");
			 } else {
				 p = Runtime.getRuntime().exec("blender -b scene.blend -P positionVertsUsingHeightmap.py -f 0");
			 }
			 waitForRenderToFinish(p);
			 if (System.getProperty("os.name").contains("win")) {
				 f = new File("C:\\tmp\\geoaleph render0000.png");
			 } else {
				 f = new File("/tmp/geoaleph render0000.png");
			 }
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return f;
	}

	private static void waitForRenderToFinish(Process p)
	{
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void randomizePosition() {
		latitude = Math.random() * 140 - 60;
		longitude = Math.random() * 360 - 180;
		System.out.println(latitude + ", " + longitude);
	}
	
	static boolean isHeightmapNotFlat(double[] heightmap) {
		for (double d : heightmap) {
			if (Math.abs(d) > 0.01) {
				return false;
			}
		}
		return true;
	}
}