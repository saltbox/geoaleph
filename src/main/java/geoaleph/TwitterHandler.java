package geoaleph;

import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;

import org.json.JSONObject;

import twitter4j.GeoLocation;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterHandler 
{
	private ConfigurationBuilder configurationBuilder;
	private Twitter twitterContext;
	
	private TwitterHandler(String accessToken, String accessTokenSecret, String consumerKey, String consumerSecret) 
	{
		configurationBuilder = new ConfigurationBuilder();
		configurationBuilder
	    	.setOAuthConsumerKey(consumerKey)
	    	.setOAuthConsumerSecret(consumerSecret)
	    	.setOAuthAccessToken(accessToken)
	    	.setOAuthAccessTokenSecret(accessTokenSecret);
		
		twitterContext = getTwitterInstance();	
	}
	
	private Twitter getTwitterInstance()
	{
		TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
	    return twitterFactory.getInstance();	
	}
	
	public void postImage(File imageFile, double latitude, double longitude) throws TwitterException
	{
		String latString = new DecimalFormat("##0.0000").format(latitude);
		String longString = new DecimalFormat("##0.0000").format(longitude);
		
		GeoLocation location = new GeoLocation(latitude, longitude);	
		StatusUpdate statusUpdate = new StatusUpdate(latString + ", " + longString);
		
		statusUpdate.setLocation(location);
		statusUpdate.setMedia(imageFile);
		statusUpdate.displayCoordinates(true);
		
		System.out.println(latString + ", " + longString);
		twitterContext.updateStatus(statusUpdate);
	}
	
	public static TwitterHandler create(JSONObject config) throws Exception
	{
		String accessToken;;
	    String accessTokenSecret;;
	    String consumerKey;
	    String consumerSecret;
		 
		if(config.has("accessToken"))
		{
			accessToken = config.getString("accessToken");
		}
		else 
		{
			throw new Exception("Config missing \"accessToken\"");
		}
		
		if(config.has("accessTokenSecret"))
		{
			accessTokenSecret = config.getString("accessTokenSecret");
		}
		else 
		{
			throw new Exception("Config missing \"accessTokenSecret\"");
		}
		
		if(config.has("consumerKey"))
		{
			consumerKey = config.getString("consumerKey");
		}
		else 
		{
			throw new Exception("Config missing \"consumerKey\"");
		}
		
		if(config.has("consumerSecret"))
		{
			consumerSecret = config.getString("consumerSecret");
		}
		else 
		{
			throw new Exception("Config missing \"consumerSecret\"");
		}
		
		return new TwitterHandler(accessToken, accessTokenSecret, consumerKey, consumerSecret);
	}
}
