package geoaleph;

public interface IHeightmapOutputter {
	public void render(double[] heightmap);
}
