package geoaleph;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class TextfileOutputter implements IHeightmapOutputter {

	public void render(double[] heightmap) {
		String outputString = "";
		
		scaleHeightmap(heightmap);

		for (double d : heightmap) {
			outputString += String.valueOf(d) + '\n';
		}
		File outputFile = new File("heightmap.txt");
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
			writer.write(outputString);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void scaleHeightmap(double[] heightmap) {
		for (int i = 0; i < heightmap.length; i++) {
			heightmap[i] /= 840;
		}
		double lowest = heightmap[0], highest = heightmap[0];
		for (int i = 0; i < heightmap.length; i++) {
			if (heightmap[i] <= Geoaleph.waterDropHigh && heightmap[i] > Geoaleph.waterDropLow) {
				heightmap[i] = Geoaleph.waterDropLow;
			}
			if (heightmap[i] < lowest) {
				lowest = heightmap[i];
			}
			if (heightmap[i] > highest) {
				highest = heightmap[i];
			}
		}
		if (lowest > 0) {
			for (int i = 0; i < heightmap.length; i++) {
				heightmap[i] -= lowest;
			}
		}
		double scaleFactor = 1f;
		if (highest > 1f) {
			scaleFactor  /= highest;
		}else if (lowest < -1f) {
			scaleFactor /= -lowest;
		}
			
		for (int i = 0; i < heightmap.length; i++) {
			heightmap[i] *= scaleFactor;
		}
		
	}
}
