package geoaleph;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageRenderer implements IHeightmapOutputter
{
	int imageType = BufferedImage.TYPE_INT_RGB;
	public void render(double[] heightMap) {
		BufferedImage image = new BufferedImage(Geoaleph.gridSize, Geoaleph.gridSize, imageType);
		for (int y = 0; y < Geoaleph.gridSize; y++) {
			for (int x = 0; x < Geoaleph.gridSize; x++) {
				image.setRGB(x, y, altitudeToRGB(heightMap[x + (y * Geoaleph.gridSize)]));
			}
		}
		Image scaledImage = image.getScaledInstance(Geoaleph.scaledImageSize, Geoaleph.scaledImageSize, Image.SCALE_DEFAULT);
		image = imageToBufferedImage(scaledImage);
		File output = new File("output.png");
		
		try
		{
			ImageIO.write(image, "png", output);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	int altitudeToRGB(double altitude) {
		int rgb = (int) (altitude < 0 ? 0 : Math.min(altitude * 1f, 255)); //red
		rgb = (int) ((rgb << 8) + Math.min(altitude * 4f + 25, 255) ); //green
		rgb = (int) ((rgb << 8) + (altitude < 0 ? 125 : 0)); //blue
		return rgb;
	}
	
	BufferedImage imageToBufferedImage(Image im) {
    BufferedImage bi = new BufferedImage
       (im.getWidth(null),im.getHeight(null),BufferedImage.TYPE_INT_RGB);
    Graphics bg = bi.getGraphics();
    bg.drawImage(im, 0, 0, null);
    bg.dispose();
    return bi;
 }
}
