package geoaleph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class HeightMapGetter
{
	public JSONObject getHeightmap(double latitude, double longitude) {
		HttpsURLConnection mapsConnection = null;
		JSONObject responseJSON = null;
		try {
			URL url = new URL("https://maps.googleapis.com/maps/api/elevation/json?locations=enc:" + getPointGrid(latitude, longitude) + "&key=AIzaSyBM_NJ4Dw1EEIWL2UIEpAgapzsQ69833KE");
			mapsConnection = (HttpsURLConnection)url.openConnection();
			mapsConnection.setRequestMethod("GET");
			int responseCode = mapsConnection.getResponseCode();
			System.out.println("sending GET request");
			System.out.println("response code: " + responseCode);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(mapsConnection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			responseJSON = new JSONObject(response.toString());
		} catch( Exception e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	String getPointGrid(double latitude, double longitude) {
		String pointGridString = "";
		Track track = new Track();
		for (int y = -Geoaleph.gridSize / 2; y < Geoaleph.gridSize / 2; y++) {
			for (int x = -Geoaleph.gridSize / 2; x < Geoaleph.gridSize / 2; x++) {
				track.addTrackpoint(new Trackpoint((latitude + Geoaleph.gridCellSize * x), (longitude + Geoaleph.gridCellSize * y)));
			}
		}
		
		HashMap<String, String> results = PolylineEncoder.createEncodings(track, 1, 1);
		return results.get("encodedPoints");
	}
	
  double[] convertJSONHeightmapToDoubleArray(JSONObject JSONHeightmap) {
		JSONArray heightmapArray = (JSONArray)JSONHeightmap.getJSONArray("results");
		double[] elevations = new double[heightmapArray.length()];
		Iterator heightmapIterator = heightmapArray.iterator();
		int index = 0;
		while (heightmapIterator.hasNext()) {
			elevations[index] = ((JSONObject)heightmapIterator.next()).getDouble("elevation");
			index++;
			heightmapIterator.remove();
		}
		return elevations;
	}
}
