import bpy;
import random;
land = bpy.data.objects['Land']
water = bpy.data.objects['Water']
landVerts = land.data.vertices.values()
even = True;
heightmapFile = open("heightmap.txt", 'r')
for i in range(len(landVerts) - 4):
    landVerts[i].co.z = 1 + float(heightmapFile.readline())

bpy.context.scene.objects.active = land
bpy.ops.object.modifier_apply(apply_as='DATA', modifier=land.modifiers[0].name)	

bpy.context.scene.objects.active = water
bpy.ops.object.modifier_apply(apply_as='DATA', modifier=water.modifiers[0].name)
water.location = ((0, 0, 0.002))
water.scale = ((1.003, 1.003, 1))

var normalAverage = Vector((0, 0, 0))

for face in land.data.polygons:
	normalAverage = normalAverage +	face.normal

normalAverage = normalAverage / len(land.data.polygons)
